#!/usr/bin/env python3

import re
import netrc
import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse
import urllib.parse
import http.cookiejar
import html.parser
import optparse
import getpass
import xml.etree.ElementTree
import datetime

class CASLoginParser(html.parser.HTMLParser):
    def __init__(self):
        html.parser.HTMLParser.__init__(self)
        self.action = None
        self.data = {}

    def handle_starttag(self, tagname, attribute):
        if tagname.lower() == 'form':
            attribute = dict(attribute)
            if 'action' in attribute:
                self.action = attribute['action']
        elif tagname.lower() == 'input':
            attribute = dict(attribute)
            if 'name' in attribute and 'value' in attribute:
                self.data[attribute['name']] = attribute['value']

class DIASAccess():
    def __init__(self, username, password):
        self.__cas_url = 'https://auth.diasjp.net/cas/login?'
        self.__username = username
        self.__password = password
        cj = http.cookiejar.CookieJar()
        self.__opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))

    def open(self, url, data=None):
        response = self.__opener.open(url, data)
        response_url = response.geturl()

        if response_url != url and response_url.startswith(self.__cas_url):
            # redirected to CAS login page
            response = self.__login_cas(response)
            if data != None:
                # If POST (data != None), need reopen
                response.close()
                response = self.__opener.open(url, data)

        return response

    def __login_cas(self, response):
        parser = CASLoginParser()
        parser.feed(str(response.read()))
        parser.close()

        if parser.action == None:
            raise LoginError('Not login page')

        action_url = urllib.parse.urljoin(response.geturl(), parser.action)
        data = parser.data
        data['username'] = self.__username
        data['password'] = self.__password

        response.close()
        response = self.__opener.open(action_url, 
                                      urllib.parse.urlencode(data).encode('utf-8'))

        if response.geturl() == action_url:
            raise LoginError('Authorization fail')

        return response

class LoginError(Exception):
    def __init__(self, e):
        Exception.__init__(self, e)

if __name__ == '__main__':
    host = 'himawari.diasjp.net'
    url = 'http://' + host + '/expert/bin/search.cgi'

    usage ='''usage: %prog [options]'''
    version='%prog 22.1017'
    parser = optparse.OptionParser(usage=usage, version=version)
    parser.add_option('-f', '--from', dest='start',
                      help='specify the start time of observation period',
                      metavar='YYYY-MM-DDThh:mm')
    parser.add_option('-t', '--to', dest='end',
                      help='specify the end time of observation period', 
                      metavar='YYYY-MM-DDThh:mm')
    parser.add_option('-T', '--type', action='append',
                      help='specify the type', 
                      metavar='{HS|NC|PI}')
    parser.add_option('-A', '--area', action='append',
                      help='specify the area', 
                      metavar='{FLDK|JP|R3}')
    parser.add_option('-B', '--band', action='append',
                      help='specify the band', 
                      metavar='Bxx')
    parser.add_option('-S', '--segment', action='append',
                      help='specify the segment', 
                      metavar='Sxx')
    parser.add_option('-p', '--parallel', action='store_true',
                      default=False,
                      help='search parallel observation data')
    parser.add_option('-n', '--netrc', default=None,
                      help='specify the netrc file', metavar='FILE')
    parser.add_option('-u', '--user', default=None,
                      help='specify the DIAS account name',
                      metavar='USERNAME')

    (options, args) = parser.parse_args()

    param = ['format=xml']

    if options.start is not None and options.end is None:
        if not re.match('\d{4}-\d{2}-\d{2}T\d{2}:\d{2}', options.start):
            parser.error('Illegal time format: ' + options.start)
        options.end = (
            datetime.datetime.strptime(options.start, '%Y-%m-%dT%H:%M') + 
            datetime.timedelta(hours=3)).strftime('%Y-%m-%dT%H:%M')
    if options.start is None and options.end is not None:
        if not re.match('\d{4}-\d{2}-\d{2}T\d{2}:\d{2}', options.end):
            parser.error('Illegal time format: ' + options.end)
        options.start = (
            datetime.datetime.strptime(options.end, '%Y-%m-%dT%H:%M') - 
            datetime.timedelta(hours=3)).strftime('%Y-%m-%dT%H:%M')

    if options.start is not None:
        param.append('start=' + options.start)
    if options.end is not None:
        param.append('end=' + options.end)

    if options.area is not None:
        param.append('area=' + ','.join(options.area))
    if options.type is not None:
        param.append('type=' + ','.join(options.type))
    if options.band is not None:
        param.append('band=' + ','.join(options.band))
    if options.segment is not None:
        param.append('segment=' + ','.join(options.segment))
 
    if options.parallel:
        param.append('concurrent=true')

    #(login, password) = (None, None)
    (login, password) = ('b36217489@gmail.com', 'bB36217489')

    '''
    try:
        auth = netrc.netrc(options.netrc).authenticators(host)
        if auth is not None:
            (login, account, password) = auth
    except (IOError):
        pass
    '''

    if options.user is not None:
        login = options.user
        password = None

    if login is None:
        login = input('Username: ')

    if password is None:
        password = getpass.getpass('Password: ')

    access = DIASAccess(login, password)
    response = access.open(url, '&'.join(param).encode('utf-8'))
    root = xml.etree.ElementTree.fromstring(response.read())
    response.close()

    lst = root.findall('item')
    for i in lst:
        print(i.attrib['id'])
